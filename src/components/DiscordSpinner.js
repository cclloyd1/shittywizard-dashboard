import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {discordColors} from "../app/theme";
import {Box} from "@material-ui/core";
import clsx from "clsx";


const useStyles = keyframeProps => makeStyles(theme => ({
    root: props => ({
        width: props.size,
        height: props.size,
        position: 'relative',
        display: 'inline-block',
    }),
    spinner: props => ({
        width: props.size*0.3125,
        height: props.size*0.3125,
        background: props.color,
        position: 'absolute',
        animationDuration: props.duration,
        animationIterationCount: 'infinite',
        animationTimingFunction: 'ease-in-out',
    }),
    spinnerAnimation: {
        animationName: '$spinners',
    },
    square2: props => ({
        animationDelay: -props.duration/2,
    }),
    '@keyframes spinners': ((props) => ({
        '25%': {
            transform: `translateX(${props.translate}px) rotate(-90deg) scale(.5)`,
        },
        '50%': {
            transform: `translateX(${props.translate}px) translateY(${props.translate}px) rotate(-180deg)`,
        },
        '75%': {
            transform: `translateX(0) translateY(${props.translate}px) rotate(-270deg) scale(.5)`,
        },
        'to': {
            transform: `rotate(-1turn)`,
        },
    }))(keyframeProps),
}));


export default function DiscordSpinner(props) {
    const {duration, size, color} = props;
    const styleProps = {
        duration: duration,
        size: size,
        color: color,
    }
    const keyframeProps = {
        translate: size*(1-0.3125),
    }
    const classes = useStyles(keyframeProps)(styleProps);

    return (
        <Box className={classes.root}>
            <Box className={clsx(classes.spinner, classes.spinnerAnimation)} />
            <Box className={clsx(classes.spinner, classes.square2, classes.spinnerAnimation)} />
        </Box>
    )

}

DiscordSpinner.defaultProps = {
    duration: 1800,
    size: 32,
    color: discordColors.blurple,
}
