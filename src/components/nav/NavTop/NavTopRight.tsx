import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import copy from 'copy-to-clipboard';
import Tooltip from "@material-ui/core/Tooltip";
import {useToasts} from "react-toast-notifications";
import {toggleDrawer} from "../../../features/app/appSlice";
import {connect} from "react-redux";


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        alignItems: 'center',
    },
    avatar: {
        width: 40,
        height: 'auto',
        borderRadius: '50%',
        cursor: 'pointer',
    },
}));


function NavTopRight(props: any) {
    const classes = useStyles();

    const { user, localToken } = props;
    const { addToast } = useToasts();

    const copyAuthToken = () => {
        copy(localToken);
        addToast('Copied token to clipboard')
    }

    return (
        <Box className={classes.root}>
            {user?.id && <Tooltip title={'Click to copy token to clipboard'} arrow>
                <img src={`https://cdn.discordapp.com/avatars/${user.discord.uid}/${user.discord.avatar}.png`}
                     className={classes.avatar}
                     onClick={copyAuthToken} alt={''} />
            </Tooltip>}
        </Box>
    );
}

const mapStateToProps = (state: any) => ({
    user: state.auth.user,
    localToken: state.auth.localToken,
})

const mapDispatchToProps = {
    toggleDrawer,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavTopRight)