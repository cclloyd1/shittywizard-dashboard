import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {appBarHeight, discordColors, drawerWidth} from "app/theme";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Drawer from "@material-ui/core/Drawer";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import {Link} from 'react-router-dom';
import Divider from "@material-ui/core/Divider";
import ListSubheader from "@material-ui/core/ListSubheader";
import {connect} from "react-redux";
import {toggleDrawer} from "features/app/appSlice";
import NavLeftLogout from "./NavLeftLogout";
import NavLeftAccount from "./NavLeftAccount";
import SoundbiteNav from "features/soundbites/nav";
import VoteNav from "features/vote/nav";
import AuthNav from "features/auth/nav";
import AppNav from "features/app/nav";


const useStyles = makeStyles(theme => ({
    root: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: discordColors.notblack,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        //...theme.mixins.toolbar,
        height: appBarHeight,
        justifyContent: 'flex-end',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    link: {
        color: 'inherit',
        textDecoration: 'none',
    }
}));

function NavLeft(props: any) {
    const classes = useStyles();

    // Redux state
    const { drawerOpen } = props;
    // Redux callbacks
    const { toggleDrawer } = props;

    return (
        <Drawer
            className={classes.root}
            variant={window.innerWidth >= 960 ? "persistent" : "temporary"}
            anchor="left"
            open={drawerOpen}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.drawerHeader}>
                <IconButton onClick={toggleDrawer}><ChevronLeftIcon /></IconButton>
            </div>
            <Divider/>
            <List>
                <AuthNav/>
                <AppNav/>
                <SoundbiteNav/>
                <VoteNav/>
            </List>
        </Drawer>
    );
}

const mapStateToProps = (state: any) => ({
    drawerOpen: state.app.drawerOpen,
})

const mapDispatchToProps = {
    toggleDrawer,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavLeft)