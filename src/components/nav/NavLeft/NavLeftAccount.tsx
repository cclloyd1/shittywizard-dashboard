import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import {connect} from "react-redux";
import {Box, Grid, TextField} from "@material-ui/core";
import {login} from "features/auth/authSlice";
import { useForm } from "react-hook-form";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0, 1),
    },
}));

type AccountFormData = {
    username: string,
    password: string,
}

function NavLeftAccount(props: any) {
    const classes = useStyles();

    const { register, handleSubmit } = useForm();

    // Redux callbacks
    const { login } = props;

    const handleLogin = (data: AccountFormData) => {
        login(data.username, data.password);
    }


    return (
        <Box className={classes.root}>
            <form id={'nav-login'} onSubmit={handleSubmit(handleLogin)}>
                <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            size={'small'}
                            placeholder={'Username'}
                            name={'username'}
                            variant={'outlined'}
                            label={'Username'}
                            inputRef={register}
                            autoComplete={'off'}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            size={'small'}
                            placeholder={'Password'}
                            name={'password'}
                            variant={'outlined'}
                            label={'Password'}
                            type={'password'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button variant={'contained'} fullWidth color={'primary'} type={'submit'}>Login</Button>
                    </Grid>
                </Grid>
            </form>
            <Divider />
        </Box>
    );
}

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    login,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavLeftAccount)

