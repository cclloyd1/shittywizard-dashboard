import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import MuiAlert from '@material-ui/lab/Alert';


const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(1),
    }
}));

export default function MUIToast(props: any) {
    const classes = useStyles();

    const { children, appearance } = props;

    return (
        <MuiAlert elevation={3} variant="filled" className={classes.root} severity={appearance} children={children} />
    );
}
