import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
// @ts-ignore
import {DefaultToastContainer} from 'react-toast-notifications';


const useStyles = makeStyles(theme => ({
    root: {
        zIndex: '5000 !important' as any,
        paddingTop: theme.mixins.toolbar.minHeight,
    },
}));


export default function MUIToastContainer(props: any) {
    const { children } = props;

    const classes = useStyles();


    return (
        <DefaultToastContainer {...props} className={classes.root} >
            {children}
        </DefaultToastContainer>
    );
}

