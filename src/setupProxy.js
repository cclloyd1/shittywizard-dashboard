const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        app.use(
            '/api', createProxyMiddleware({
                target: 'http://localhost:8000',
                changeOrigin: true,
            })
        );
        app.use(
            '/staticfiles', createProxyMiddleware({
                target: 'http://localhost:8000',
                changeOrigin: true,
            })
        );
        app.use(
            '/admin', createProxyMiddleware({
                target: 'http://localhost:8000',
                changeOrigin: true,
            })
        );
        app.use(
            '/auth', createProxyMiddleware({
                target: 'http://localhost:8000',
                changeOrigin: true,
            })
        );
        app.use(
            '/social', createProxyMiddleware({
                target: 'http://localhost:8000',
                changeOrigin: true,
            })
        );
        app.use(
            '/media', createProxyMiddleware({
                target: 'http://localhost:8000',
                changeOrigin: true,
            })
        );
    }

};