import {Button} from "@material-ui/core";
import React, {useState} from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import {useVote} from "./VoteProvider";


export default function EndVote() {
    const { endVote, vote } = useVote();

    const [open, setOpen] = useState<boolean>(false);

    const toggleOpen = () => {
        setOpen(!open);
    }


    return (
        <>
            <Button color={'primary'} onClick={toggleOpen}>End Vote</Button>
            <Dialog open={open} onClose={toggleOpen}>
                <DialogTitle>{"Confirm"}</DialogTitle>
                <DialogContent>
                    <DialogContentText>Are you sure you want to end the current vote?</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => endVote(vote.id)} color="primary">Yes, end it</Button>
                    <Button onClick={toggleOpen} color="primary" autoFocus>Cancel</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}