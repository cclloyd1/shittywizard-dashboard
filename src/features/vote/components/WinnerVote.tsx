import {Button} from "@material-ui/core";
import React, {useState} from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import {useVote} from "./VoteProvider";


export default function WinnerVote() {
    const { declareWinner, vote } = useVote();

    const [open, setOpen] = useState<boolean>(false);

    const toggleOpen = () => {
        setOpen(!open);
    }


    return (
        <>
            <Button fullWidth color={'primary'} onClick={toggleOpen}>Winner</Button>
            <Dialog open={open} onClose={toggleOpen}>
                <DialogTitle>{"Confirm"}</DialogTitle>
                <DialogContent>
                    <DialogContentText>Declare winner early?</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => declareWinner(vote.id)} color="primary">Confirm</Button>
                    <Button onClick={toggleOpen} color="primary" autoFocus>Cancel</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}