import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Divider, IconButton, Tooltip} from "@material-ui/core";
import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import clsx from "clsx";
import AddVote from "../AddVote";
import {useVote} from "../VoteProvider";


const useStyles = makeStyles((theme: any) => ({
    root: {
        padding: theme.spacing(2),
        opacity: 0.35,
        transitionProperty: 'opacity',
        transitionDuration: theme.transitions.duration.short,
        '&:hover': {
            opacity: 1,
        }
    },
    active: {
        opacity: 1,
    },
    end: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
}));

export default function VoteChannelTerminated() {
    const classes = useStyles();

    const { channel, vote, addVote } = useVote();

    const [addActive, setAddActive] = useState<boolean>(false);

    const toggleAddActive = () => {
        setAddActive(!addActive);
    }


    return (
        <Paper className={clsx(classes.root, {
            [classes.active]: addActive,
        })}>
            <Grid container spacing={1}>
                <Grid item xs={9}>
                    <Typography variant={'subtitle2'}>#{channel.name}</Typography>
                    <Typography variant={'h5'}>{vote.title}</Typography>
                </Grid>
                <Grid item xs={3} className={classes.end}>
                    <Tooltip title={'Start new vote'}>
                        <IconButton onClick={toggleAddActive}><AddCircleIcon/></IconButton>
                    </Tooltip>
                </Grid>
                <Grid item xs={12}><Divider/></Grid>
                <Grid item xs={12}>
                    {!vote.started && <Typography variant={'body2'}>This vote was ended early.</Typography>}
                    {vote.started && vote.winner.length == 0 && <Typography variant={'body2'}>This vote timed out without any options added.</Typography>}
                </Grid>

                <Grid item xs={12}>
                    <AddVote open={addActive} toggleOpen={toggleAddActive}/>
                </Grid>

            </Grid>
        </Paper>

    )
}