import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {
    Button, Collapse,
    Divider,
    IconButton,
    TextField, Tooltip
} from "@material-ui/core";
import dateformat from "dateformat";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import React, {useState} from "react";
import {timeDifference} from "../VoteCategory";
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import {api} from "features/auth/authSlice";
import {connect} from "react-redux";
import {useToasts} from "react-toast-notifications";
import {useForm} from "react-hook-form";
import EndVote from "../EndVote";
import StartVote from "../StartVote";
import {useVote} from "../VoteProvider";
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import {DateTimePicker} from "@material-ui/pickers";
import add from "date-fns/add";
import sub from "date-fns/sub";
import endOfWeek from "date-fns/endOfWeek";
import compareAsc from "date-fns/compareAsc";


const useStyles = makeStyles((theme: any) => ({
    root: {
        padding: theme.spacing(2),
    },
    choices: {
        margin: 0,
        paddingLeft: theme.spacing(3),
        fontSize: 16,
    },
    dates: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    add: {
        fontSize: 16,
        display: 'flex',
        alignItems: 'center',
    },
    end: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    divider: {
        //marginTop: theme.spacing(0.25),
        //marginBottom: theme.spacing(0.5),
    },
    separate: {
        display: 'flex',
        justifyContent: 'space-between',
    },
}));

export default function VoteChannelPreVoteView() {
    const classes = useStyles();
    const { vote, channel, editActive, toggleEdit } = useVote();


    // TODO: set winners to list users
    //  InProgress: custom view to get current votes (will have to query discord)
    //  InProgress show time remaining
    //  Winner: show how long ago it ended
    //  Set base vote (index.tsx) to use noVote if vote is over 1 week old


    return (
        <Paper className={classes.root}>
            <Grid container spacing={1}>

                <Grid item md={9} xs={editActive ? 8 : 9}>
                    <Typography variant={'subtitle2'} gutterBottom={editActive}>#{channel.name}</Typography>
                    <Typography variant={'h5'}>{vote.title}</Typography>
                </Grid>
                <Grid item md={3} xs={editActive ? 4 : 3} className={classes.end}>
                    <Tooltip title={'Edit Vote'}>
                        <IconButton onClick={toggleEdit}><EditIcon/></IconButton>
                    </Tooltip>
                </Grid>
                <Grid item xs={12}><Divider/></Grid>



                <Grid item xs={12}>
                    <Typography variant={'body1'} className={classes.dates}>
                        <span><b>Start:</b> {dateformat(new Date(vote.start), 'yyyy-mm-dd H:MM Z')}</span>
                        <span><b>Duration:</b> {timeDifference(vote.end, vote.start)}</span>
                    </Typography>
                </Grid>
                {vote.choices.length > 0 && <Grid item xs={12}>
                    <ol className={classes.choices}>
                        {vote.choices.map((c: string, i: number) => <li key={i}>{c}</li>)}
                    </ol>
                </Grid>}
                {vote.choices.length === 0 && <Grid item xs={12}>
                    <Typography variant={'body2'}>No choices have been added to this vote.</Typography>
                </Grid>}


                <Grid item xs={12} className={classes.separate}>
                        <StartVote />
                        <EndVote />
                </Grid>

            </Grid>
        </Paper>
    )
}
