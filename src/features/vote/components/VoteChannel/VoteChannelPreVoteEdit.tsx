import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {
    Button, Collapse,
    Divider,
    IconButton,
    TextField, Tooltip
} from "@material-ui/core";
import dateformat from "dateformat";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import React, {useEffect, useState} from "react";
import {timeDifference} from "../VoteCategory";
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import {api} from "features/auth/authSlice";
import {connect} from "react-redux";
import {useToasts} from "react-toast-notifications";
import {useForm} from "react-hook-form";
import EndVote from "../EndVote";
import StartVote from "../StartVote";
import {useVote} from "../VoteProvider";
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import {DateTimePicker} from "@material-ui/pickers";
import add from "date-fns/add";
import sub from "date-fns/sub";
import endOfWeek from "date-fns/endOfWeek";
import compareAsc from "date-fns/compareAsc";
import {parseISO} from "date-fns";


const useStyles = makeStyles((theme: any) => ({
    root: {
        padding: theme.spacing(2),
    },
    choices: {
        margin: 0,
        paddingLeft: theme.spacing(3),
        fontSize: 16,
    },
    dates: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    add: {
        fontSize: 16,
        display: 'flex',
        alignItems: 'center',
    },
    end: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    divider: {
        //marginTop: theme.spacing(0.25),
        //marginBottom: theme.spacing(0.5),
    },
    separate: {
        display: 'flex',
        justifyContent: 'space-between',
    },
}));

function VoteChannelPreVoteEdit(props: any) {
    const classes = useStyles();
    const { addToast } = useToasts();
    const { register, handleSubmit, getValues } = useForm();
    const { channel, updateVote, editActive, toggleEdit } = useVote();
    const propVote = useVote().vote;
    const setPropVote = useVote().setVote;

    const { api } = props;

    const [addActive, setAddActive] = useState(false);
    const [vote, setVote] = useState<any>(propVote);

    // TODO: Move remove vote option to provider

    useEffect(() => {
        setVote(propVote);
    }, [propVote])

    const toggleAddActive = () => {
        setAddActive(!addActive);
    }

    // TODO: set winners to list users
    //  InProgress: custom view to get current votes (will have to query discord)
    //  InProgress show time remaining
    //  Winner: show how long ago it ended
    //  Set base vote (index.tsx) to use noVote if vote is over 1 week old

    const addVoteOption = (data: any) => {
        console.log(data)
        console.log(getValues());
        api(`/api/vote/${vote.id}/`, {
            method: 'PUT',
            data: {
                ...vote,
                choices: [
                    ...vote.choices,
                    getValues('choice'),
                ]
            },
        }).then((data: any) => {
            console.log('New choice data', data);
            setVote(data);
            toggleAddActive();
        }).catch((err: any) => {
            console.error('Error adding vote', err);
            addToast(err.detail, {appearance: 'error'})
        });
    }

    const handleSave = () => {
        const data = getValues();
        console.log('data', data);
        //const success = updateVote(data);
        //if (success)
        //    toggleEditActive();
    }

    // Default date is Friday@Noon, and Saturday@Noon
    const [start, setStart] = useState<Date>(parseISO(vote.start));
    const [end, setEnd] = useState<Date>(parseISO(vote.end));

    const handleStartChange = (value: any) => {
        setStart(value);
        if (compareAsc(value, end) >= 0) {
            setEnd(add(value, {days: 1}));
        }
    }

    const handleEndChange = (value: any) => {
        setEnd(value);
    }

    // TODO: this is removing the last index instead of what I want to remove
    const removeOption = (index: number) => {
        let choices = vote.choices;
        choices.splice(index, 1);
        setVote({
            ...vote,
            choices: [...choices],
        })
    }

    return (
        <Paper className={classes.root}>
            <form id={`vote-${vote.id}`}>
                <Grid container spacing={1}>

                    {/* Top section */}
                    <Grid item md={9} xs={editActive ? 8 : 9}>
                        <Typography variant={'subtitle2'} gutterBottom={editActive}>#{channel.name}</Typography>
                        <TextField
                            fullWidth
                            name={'title'}
                            defaultValue={'vote.title'}
                            variant={'outlined'}
                            size={'small'}
                            label={'Vote Title'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md={3} xs={editActive ? 4 : 3} className={classes.end}>
                        <Tooltip title={'Save Changes'}>
                            <IconButton onClick={handleSave}><SaveIcon/></IconButton>
                        </Tooltip>
                        <Tooltip title={'Cancel'}>
                            <IconButton onClick={toggleEdit}><CloseIcon/></IconButton>
                        </Tooltip>
                    </Grid>
                    <Grid item xs={12}><Divider/></Grid>


                    {/* Date section */}
                    <Grid item xs={12}>
                        <Grid container spacing={1}>
                            <Grid item xs={12} md={6}>
                                <DateTimePicker
                                    value={start}
                                    onChange={handleStartChange}
                                    disablePast
                                    label={'Start'}
                                    name={'start'}
                                    size={'small'}
                                    showTodayButton
                                    inputVariant={'outlined'}
                                    minutesStep={5}
                                    inputRef={register}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <DateTimePicker
                                    value={end}
                                    fullWidth
                                    onChange={handleEndChange}
                                    disablePast
                                    label={'End'}
                                    name={'end'}
                                    size={'small'}
                                    showTodayButton
                                    inputVariant={'outlined'}
                                    minutesStep={5}
                                    minDate={add(start, {minutes: 5})}
                                    inputRef={register}
                                />
                            </Grid>
                        </Grid>
                    </Grid>

                    {/* Choices list */}
                    {vote.choices.length > 0 && <Grid item xs={12}><Grid container spacing={0}>
                        {vote.choices.map((c: string, i: number) => <React.Fragment key={i}>
                            <Grid item xs={10}>
                                <TextField
                                    fullWidth
                                    name={`choices[${i}]`}
                                    defaultValue={c}
                                    variant={'filled'}
                                    size={'small'}
                                    label={`Option ${i+1}`}
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Tooltip title={'Delete Option'}>
                                    <IconButton onClick={() => removeOption(i)}><DeleteIcon/></IconButton>
                                </Tooltip>
                            </Grid>
                        </React.Fragment>)}
                    </Grid></Grid>}
                    {vote.choices.length === 0 && <Grid item xs={12}>
                        <Typography variant={'body2'}>No choices have been added to this vote.</Typography>
                    </Grid>}

                    {/* Choices add area */}
                    <Grid item xs={12}>
                        {!addActive && <>
                            {vote.choices.length < 10 && <Button startIcon={<AddCircleIcon/>} onClick={toggleAddActive}>Add Option</Button>}
                            {vote.choices.length >= 10 && <Typography variant={'body2'}><em>You cannot add more options to this vote.</em></Typography>}
                        </>}
                        {addActive && <Grid container spacing={1}>
                            <Grid item xs={9}>
                                <TextField
                                    fullWidth
                                    variant={'outlined'}
                                    placeholder={'New vote option'}
                                    size={'small'}
                                    name={'choice'}
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <Button fullWidth variant={'contained'} color={'primary'} onClick={addVoteOption}>Add</Button>
                            </Grid>
                        </Grid>}
                    </Grid>

                </Grid>
            </form>
        </Paper>
    )
}

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    api,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VoteChannelPreVoteEdit)