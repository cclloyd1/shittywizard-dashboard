import React from "react";
import {useVote} from "../VoteProvider";
import VoteChannelPreVoteView from "./VoteChannelPreVoteView";
import VoteChannelPreVoteEdit from "./VoteChannelPreVoteEdit";


export default function VoteChannelPreVote() {
    const { editActive } = useVote();


    // TODO: set winners to list users
    //  InProgress: custom view to get current votes (will have to query discord)
    //  InProgress show time remaining
    //  Winner: show how long ago it ended
    //  Set base vote (index.tsx) to use noVote if vote is over 1 week old


    return (
        <>
            {!editActive && <VoteChannelPreVoteView/>}
            {editActive && <VoteChannelPreVoteEdit/>}
        </>
    )
}
