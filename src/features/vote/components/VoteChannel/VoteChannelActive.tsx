import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Divider, Hidden, IconButton, Tooltip} from "@material-ui/core";
import dateformat from "dateformat";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import EndVote from "../EndVote";
import WinnerVote from "../WinnerVote";
import {useVote} from "../VoteProvider";
import EditIcon from "@material-ui/icons/Edit";


const useStyles = makeStyles((theme: any) => ({
    root: {
        padding: theme.spacing(2),
        boxShadow: 'inset 0 0 15px rgba(255,255,255,0.3)',
    },
    choices: {
        margin: 0,
        paddingLeft: theme.spacing(3),
        fontSize: 16,
    },
    dates: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    add: {
        fontSize: 16,
        display: 'flex',
        alignItems: 'center',
    },
    end: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    separate: {
        display: 'flex',
        justifyContent: 'space-between',
    },
}));

export default function VoteChannelActive(props: any) {
    const classes = useStyles();
    const { channel, vote } = useVote();


    return (
        <Paper className={classes.root}>
            <Grid container spacing={1}>

                <Grid item xs={12}>
                    <Typography variant={'subtitle2'}>#{channel.name}</Typography>
                    <Typography variant={'h5'}>{vote.title}</Typography>
                </Grid>
                <Grid item xs={12}><Divider/></Grid>



                <Grid item xs={12}>
                    <Typography variant={'body1'} className={classes.dates}>
                        <span><b>VOTE IN PROGRESS</b></span>
                        <span><b>Ends:</b> {dateformat(new Date(vote.end), 'yyyy-mm-dd H:MM Z')}</span>
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <ol className={classes.choices}>
                        {vote.choices.map((c: string) => <li key={c}>{c}</li>)}
                    </ol>
                </Grid>



                <Grid item xs={12} className={classes.separate}>
                    <WinnerVote />
                    <EndVote/>
                </Grid>

            </Grid>
        </Paper>
    )
}