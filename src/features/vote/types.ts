

export interface Vote {
    id: number,
    channel?: string,
    message?: string,
    winner: number[],
    choices: string[],
    emoji: string[],
    started: boolean,
    finished: boolean,
    created: string,
    modified: string,
    deleted: string,
}

export const VOTE_STATUS = {
    PREVOTE: 0,
    ACTIVE: 1,
    FINISHED: 2,
    TERMINATED: 3,
    NOVOTE: 4,
}