import React, {useEffect} from 'react';
import {fade, makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import clsx from "clsx";
import {appBarHeight, discordColors} from "app/theme";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import AddCircle from '@material-ui/icons/AddCircle';
import {useForm} from "react-hook-form";
import {useToasts} from "react-toast-notifications";
import CircularProgress from "@material-ui/core/CircularProgress";
import FullPageLayout from "components/FullPageLayout";
import SoundbiteCollection from "features/soundbites/components/SoundbiteCollection";
import {connect} from "react-redux";
import {Soundbite, SoundbiteCategory} from "features/soundbites/types";
import {api} from "../../auth/authSlice";
import {Hidden} from "@material-ui/core";


const useStyles = makeStyles((theme: any) => ({
    root: {
        height: `calc(100vh - ${appBarHeight}px - ${theme.spacing(2)}px)`,
    },
    centeredGrid: {
        display: 'flex',
        alignItems: 'center',
    },
    accordion: {
        backgroundColor: 'transparent',
        transitionProperty: 'background-color',
        transitionDuration: theme.transitions.duration.standard,
        boxShadow: 'none',
        borderRadius: 3,
        '&:before': {
            opacity: 0,
        },

    },
    accordionHover: {
        '&:hover': {
            backgroundColor: fade(discordColors.grey[300], 0.5),
        },
    },
    accordionActive: {
        boxShadow: 'inherit',
        backgroundColor: discordColors.grey[300],
    },
    accordionDisabled: {
        backgroundColor: 'transparent !important',
    },
    header: {
        display: 'flex',
        alignItems: 'center',
    },
    expandIcon: {
        marginRight: theme.spacing(1),
    },
    loadingBox: {
        display: 'flex',
        justifyContent: 'center',
        //alignItems: "start",
        padding: theme.spacing(3),
    },
    loadingIcon: {
        color: discordColors.blurple,
    },
}));

function PageVoteNew(props: any) {
    const classes = useStyles();

    const { register, watch, handleSubmit } = useForm();
    const { addToast } = useToasts();

    const { api } = props;

    //const [soundbites, {set: setSoundbites, }] = useArray([]);
    // TODO: Make as a useArray hook instead
    const [soundbites, setSoundbites] = React.useState<any>([]);
    const [expanded, setExpanded] = React.useState<any>('');
    const [loading, setLoading] = React.useState<boolean>(true);


    useEffect(() => {
        document.title = `DiscordBot · Soundbites`;
    }, []);

    const fetchAllSoundbites = () => {
        api('/api/soundbite/category/all/').then((data: any) => {
            console.log('get all categories', data);
            setSoundbites([...data]);
            setLoading(false);
        }).catch((err: any) => {
            console.error('error', err);
            addToast(err.detail, {appearance: 'error'});
        });
    }

    useEffect(() => {
        fetchAllSoundbites();
    }, []);

    const handleAccordionChange = (panel: any) => (event: any, isExpanded: any) => {
        setExpanded(isExpanded ? panel : false);
    };

    const addSoundbite = (data: any, e: any) => {
        let formdata = new FormData();
        for (let key in data) {
            if (key !== 'audio')
                formdata.append(key, data[key]);
            else {
                formdata.append('audio', data.audio[0]);
            }
        }

        console.log('form data', data)

        api('/api/soundbite/soundbite/', {
            method: 'POST',
            body: formdata,
        }).then((data: any) => {
            fetchAllSoundbites();
        }).catch((err: any) => {
            console.error(err);
            addToast('Error adding soundbite.', {appearance: 'error'});
        })
    }


    return (
        <FullPageLayout header={'Soundbites'}>
            <Accordion
                disabled={loading}
                expanded={expanded === 'new'}
                onChange={handleAccordionChange('new')}
                className={clsx(classes.accordion, {
                    [classes.accordionActive]: expanded,
                    [classes.accordionHover]: !expanded,
                })}
                classes={{
                    disabled: classes.accordionDisabled,
                }}
            >
                <AccordionSummary >
                    <Typography variant={'h5'} className={classes.header}>
                        {expanded === 'new' ? <ExpandLessIcon className={classes.expandIcon}/>:<AddCircle className={classes.expandIcon}/>} Add New
                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <form onSubmit={handleSubmit(addSoundbite)}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={2}>
                                <TextField
                                    variant={'outlined'}
                                    fullWidth
                                    name={'name'}
                                    placeholder={'Unique Name'}
                                    label={'Name'}
                                    inputRef={register({})}
                                    required={true}
                                />
                            </Grid>
                            <Grid item xs={12} md={3}>
                                <TextField
                                    variant={'outlined'}
                                    fullWidth
                                    name={'category'}
                                    placeholder={'Uncategorized'}
                                    label={'Category'}
                                    inputRef={register({})}
                                    required={true}
                                />
                            </Grid>
                            <Grid item xs={12} md={5}>
                                <TextField
                                    fullWidth
                                    variant={'outlined'}
                                    name={'audio'}
                                    label={'Audio File'}
                                    //defaultValue={'No file chosen'}
                                    placeholder={'Audio File'}
                                    inputRef={register({})}
                                    required={true}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    inputProps={{
                                        type: 'file',
                                        id: `audio`,
                                        name: 'audio',
                                        accept: 'audio/mpeg, audio/aac',
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} md={2} className={classes.centeredGrid}>
                                <Button fullWidth variant={'contained'} color={'primary'} type={'submit'}>Add</Button>
                            </Grid>
                            <Grid item xs={12} md={12}>
                                <TextField
                                    variant={'outlined'}
                                    fullWidth
                                    name={'transcript'}
                                    placeholder={'Full transcript'}
                                    label={'Transcript'}
                                    inputRef={register({})}
                                />
                            </Grid>
                        </Grid>
                    </form>
                </AccordionDetails>
            </Accordion>
            {!loading && <Box>
                {soundbites.map((c: SoundbiteCategory) =>
                    <SoundbiteCollection title={c.name} soundbites={c.soundbites} id={c.id} key={c.id} />
                )}
            </Box>}
            {loading && <Box className={classes.loadingBox}>
                <CircularProgress className={classes.loadingIcon} size={50} />
            </Box>}
        </FullPageLayout>

    );

}

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    api,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageVoteNew)