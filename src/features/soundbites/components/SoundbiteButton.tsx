import * as React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import {useToasts} from "react-toast-notifications";
import {discordColors} from "../../../app/theme";
import {api, loginWithDiscord} from "../../auth/authSlice";
import {connect} from "react-redux";
import {green} from "@material-ui/core/colors";
import {Box, Fade, Grow, Zoom} from "@material-ui/core";
import DoneIcon from '@material-ui/icons/Done';


const useStyles = makeStyles(theme => ({
    soundbite: {
        margin: theme.spacing(1),
        backgroundColor: discordColors.grey[200],
        color: discordColors.grey.text,
        textTransform: 'none',
        padding: theme.spacing(1, 3),
        borderRadius: theme.spacing(0.75),
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: discordColors.grey[100],
        },
        overflow: 'hidden',
        position: 'relative',
        //width: 100,
    },
    buttonOverlay: {
        backgroundColor: green[600],
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'default',
    },
    overlayIcon: {
        height: '100%',
        width: 'auto',
    },
}));

function SoundbiteButton(props: any) {
    const classes = useStyles();
    const { addToast } = useToasts();

    const { transcript, id, name } = props;
    const { api } = props;

    const [completed, setCompleted] = React.useState<boolean>(false);


    const handlePlay = async (id: number) => {
        if (completed)
            return;
        api(`/api/soundbite/soundbite/${id}/play/`).then(() => {
            setCompleted(true);
            new Promise(r => setTimeout(r, 1500)).then(() => {
                setCompleted(false);
            });
        }).catch((err: any) => {
            addToast('Error playing soundbite.', {appearance: 'error'})
        })
    }

    return (
        <Tooltip enterDelay={1000} title={transcript}>
            <Button disableRipple={completed} className={classes.soundbite} variant={'contained'} onClick={() => handlePlay(id)}>
                <Typography variant={'h6'}>{name}</Typography>
                <Grow in={completed} timeout={{ enter: 250, exit: 250,}}>
                    <Box className={classes.buttonOverlay}><DoneIcon className={classes.overlayIcon}/></Box>
                </Grow>
            </Button>
        </Tooltip>
    );
}

SoundbiteButton.defaultProps = {
    id: 0,
    name: '',
    transcript: '',
};

SoundbiteButton.propTypes = {

};

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    api,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SoundbiteButton)