import * as React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import PropTypes from 'prop-types';


const useStyles = makeStyles(theme => ({
    text: {
        marginTop: theme.spacing(3),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
}));

export default function SoundbiteCategoryHeader(props: any) {
    const classes = useStyles();
    const { title } = props;

    return (
        <>
            <Typography variant={'h5'} className={classes.text}>
                {title}
            </Typography>
            <Divider className={classes.divider}/>
        </>
    );
}

SoundbiteCategoryHeader.defaultProps = {
    title: '',
};

SoundbiteCategoryHeader.propTypes = {

};