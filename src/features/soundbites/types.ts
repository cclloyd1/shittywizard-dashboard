
export interface SoundbiteCategory {
    id: number;
    name: string;
    default: boolean;
    soundbites: Soundbite[];
    created: Date;
    modified: Date;
    deleted: Date;
}

export interface Soundbite {
    id: number;
    name: string;
    category: SoundbiteCategory;
    owner: string;
    transcript: string;
    url: string;
    audio: string;
    duration: number;
    created: Date;
    modified: Date
    deleted: Date;
}