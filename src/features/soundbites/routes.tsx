import React from "react";
import ProtectedRoute from "components/ProtectedRoute";
import PageSoundbites from "./pages/PageSoundbites";


const SoundbiteRoutes = (
    <>
        <ProtectedRoute path='/soundbites' exact><PageSoundbites/></ProtectedRoute>
    </>
)

export default SoundbiteRoutes;

