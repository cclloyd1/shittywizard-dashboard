import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import FullPageLayout from "components/FullPageLayout";
import {api, getUserInfo, login} from "features/auth/authSlice";
import {connect} from "react-redux";
import {Button} from "@material-ui/core";


const useStyles = makeStyles(theme => ({

}));

function PageHome(props: any) {
    const classes = useStyles();

    //const { isAuthenticated, discordAPI } = useAuth();
    const { isAuthenticated, getUserInfo, api, API } = props;

    useEffect(() => {
        /*
        if (isAuthenticated)
            discordAPI('/users/@me').then(data => {
                console.log('discord data', data);
            })*/
    }, []);

    return (
        <FullPageLayout
            header={'Discord Bot Dashboard'}
            title={'Home'}
        >
            <Typography variant={'body1'}>This is the dashboard for your Discord bot.  Pages you add will show up in the left side menu.  Each page is self contained.  You must be logged in with discord to access anything.</Typography>

        </FullPageLayout>
    );

}

const mapStateToProps = (state: any) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = {
    login,
    getUserInfo,
    api,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageHome)