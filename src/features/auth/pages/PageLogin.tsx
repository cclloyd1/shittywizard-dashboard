import React, {useEffect} from 'react';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {lighten} from "@material-ui/core";
import FullPageLayout from "components/FullPageLayout";
import {connect} from "react-redux";
import {Redirect, useHistory} from 'react-router-dom';
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import {makeStyles} from "@material-ui/core/styles";
import {discordColors} from "app/theme";


const useStyles = makeStyles(theme => ({
    header: {
        marginBottom: theme.spacing(4),
    },
    ssoLogin: {},
    ssoLoginIcon: {
        height: theme.spacing(5),
        width: 'auto',
        display: 'inline-block',
    },
    discordButton: {
        margin: 'auto',
        display: 'block',
        marginBottom: theme.spacing(2),
        textAlign: 'center',
        backgroundColor: discordColors.blurple,
        '&:hover': {
            backgroundColor: lighten(discordColors.blurple, 0.1),
        },
    },
    buttonLink: {
        width: '100%',
        height: 40,
        textAlign: 'center',
        textDecoration: 'none',
        color: discordColors.white,
        fontSize: 16,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
}));


function PageLogin(props: any) {
    const classes = useStyles();

    const history = useHistory();

    const { isAuthenticated } = props;
    const [clientID, setClientID] = React.useState(0);

    let port = `:${window.location.port}`
    if (window.location.port == '' || window.location.port == '80' || window.location.port == '443')
        port = ''

    const hostname = encodeURIComponent(`${window.location.protocol}//${window.location.hostname}${port}/authorize`)
    console.log('hostname', hostname, `${window.location.protocol}//${window.location.hostname}${port}/authorize`);
    // TODO: Figure out why this is broken in prod (not a well formed url)
    const discord_oauth_url = `https://discord.com/api/oauth2/authorize?client_id=${clientID}&redirect_uri=${hostname}&response_type=code&scope=identify%20email`;
    const discord_add_bot_url = `https://discord.com/api/oauth2/authorize?client_id=${clientID}&permissions=8&redirect_uri=${hostname}&scope=bot`;

    // Go to previous page once successfully logged in
    useEffect(() => {
        if (isAuthenticated)
            history.goBack();
    }, [isAuthenticated])

    useEffect(() => {
        fetch('/social/discord/oauth/client/').then(r => {
            if (!r.ok)
                throw r;
            else
                return r.json();
        }).then(data => {
            setClientID(data);
        }).catch(err => {
            try {
                console.error('Error fetching django token.', err.json());
            }
            catch {
                console.error('Error fetching django token.', err);
            }
        })
    }, []);

    return (
        <FullPageLayout title={'Login'} maxWidth={'sm'} paper middle padding={6}>
            <Typography variant={'h4'} className={classes.header}>Login with Discord.</Typography>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Button fullWidth className={clsx(classes.discordButton, classes.ssoLogin)}>
                        <a href={discord_oauth_url} target={'_SELF'} className={classes.buttonLink}>
                            <img src={'https://discord.com/assets/192cb9459cbc0f9e73e2591b700f1857.svg'} className={classes.ssoLoginIcon} alt={'login'}/>
                        </a>
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    <Button fullWidth className={clsx(classes.discordButton, classes.ssoLogin)}>
                        <a href={discord_add_bot_url} target={'_BLANK'} className={classes.buttonLink}>Add bot to server</a>
                    </Button>
                </Grid>
            </Grid>
        </FullPageLayout>

    );

}


const mapStateToProps = (state: any) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = {

}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageLogin)
