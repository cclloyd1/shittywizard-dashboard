import { createSlice } from '@reduxjs/toolkit'
import axios from "axios";
import jwt_decode from "jwt-decode";

const guestUser = {
    id: 0,
    name: 'Guest',
}
/*
interface JWT {
    access_token: string;
    refresh_token: string;
    expires_in: number;
    scope: string;
    token_type: string;
}*/


const authSlice = createSlice({
    name: 'auth',
    initialState: {
        isAuthenticated: false,
        discordToken: null,
        localToken: null,
        user: null,
        profiles: [],
        errors: {},
    },
    reducers: {
        logoutSuccess: (state, action) => {
            state.isAuthenticated = false;
            state.localToken = null;
            state.discordToken = null;
            state.user = null;
        },
        loginSuccess: (state, action) => {
            state.access = {
                token: action.payload.access,
                ...jwt_decode(action.payload.access),
            }
            state.refresh = {
                token: action.payload.refresh,
                ...jwt_decode(action.payload.refresh),
            }
            state.isAuthenticated = true;
        },
        refreshSuccess: (state, action) => {
            state.access = {
                token: action.payload.access,
                ...jwt_decode(action.payload.access),
            }
            state.refresh = {
                token: action.payload.refresh,
                ...jwt_decode(action.payload.refresh),
            }
        },
        setLocalTokenSuccess: (state, action) => {
            state.localToken = action.payload;
            state.isAuthenticated = true;
        },
        setDiscordTokenSuccess: (state, action) => {
            state.discordToken = action.payload;
        },
        setUserSuccess: (state, action) => {
            state.user = action.payload;
        },
    },
});

export const exchangeToken = (token) => async (dispatch) => {
    console.log('exchanging token');
    const res = await fetch('/social/discord/', {
        method: 'POST',
        body: JSON.stringify(token),
        headers: {
            'Content-Type': 'application/json'
        },
    })
    if (!res.ok) {
        console.error('Error fetching Django token')
        throw await res.json();
    }

    const data = await res.json();
    console.log('Exchange token data', data);
    dispatch(setLocalTokenSuccess(data.token))
    dispatch(getUserInfo());
}

export const loginWithDiscord = (code) => async (dispatch) => {
    const res = await fetch('/social/oauth/token/', {
        method: 'POST',
        body: JSON.stringify({code: code}),
        headers: {
            'Content-Type': 'application/json'
        },
    })

    if (!res.ok) {
        console.error('Error exchanging discord token');
        let err = null;
        try {
            err = await res.json();
        }
        catch {
            err = {msg: 'Error exchanging discord token'}
        }
        throw err;
    }
    const data = await res.json();
    console.log('Discord token data', data);
    dispatch(setDiscordTokenSuccess(data));
    console.log('finished discord token dispatch');
    dispatch(exchangeToken(data));
}


export const getUserInfo = () => async (dispatch, getState) => {
    if (!getState().auth.isAuthenticated)
        throw {detail: 'Unable to fetch user data:  Not logged in.'}

    //const user = dispatch(api({url: '/api/user/me/'}))
    const user = await apiCall({url: '/api/user/me/'}, getState().auth.localToken);
    console.log('api user', user);
    dispatch(setUserSuccess(user));
    return user;
}


const refreshToken = async (token) => {
    const res = await axios({
        method: 'POST',
        url: '/api/auth/token/refresh/',
        headers: {'Content-Type': 'application/json'},
        data: {refresh: token},
    }).catch(err => {
        console.error('Error refreshing token', err);
        throw err;
    });
    return res.data;
}


// Internal use API call, so you don't have to use a dispatch, but token must be passed along with it.
const apiCall = async (options, token) => {
    if (token) {
        if (!options.headers)
            options.headers = {}
        // Do request if not expired
        const res = await axios({
            ...options,
            headers: {
                ...options.headers,
                Authorization: `Token ${token}`,
            }
        }).catch((err) => {
            if (!err.response?.data?.detail)
                throw {detail: err.response.statusText};
            else
                throw err.response.data;
        });
        return res.data;
    }
    else {
        const res = await axios(options).catch((err) => {
            if (!err.response?.data?.detail)
                throw {detail: err.response.statusText};
            else
                throw err.response.data;
        });
        return res.data;
    }
}


export const api = (url, options={}) => async (dispatch, getState) => {
    let state = getState().auth;

    // Do authenticated API request
    if (state.isAuthenticated) {
        const res = await axios({
            url: url,
            ...options,
            headers: {
                ...options?.headers,
                Authorization: `Token ${state.localToken}`,
            }
        }).catch((err) => {
            if (!err.response?.data?.detail)
                throw {detail: err.response.statusText};
            else
                throw err.response.data;
        });
        return res.data;
    }
    // Do unauthenticated API request
    else {
        const res = await axios(options).catch((err) => {
            if (!err.response?.data?.detail)
                throw {detail: err.response.statusText};
            else
                throw err.response.data;
        });
        return res.data;
    }
}


export const login = (username, password) => async (dispatch) => {
    axios({
        method: 'POST',
        url: '/api/auth/token/obtain/',
        headers: {
            'Content-Type': 'application/json',
        },
        data: {
            username: username,
            password: password,
        }
    }).then(res => {
        console.log(res);
        dispatch(loginSuccess(res.data));
    }).catch(err => {
        console.error(err);
    });
}

export const logout = () => async (dispatch) => {
    dispatch(logoutSuccess())
}

export const {
    logoutSuccess,
    loginSuccess,
    refreshSuccess,
    setLocalTokenSuccess,
    setDiscordTokenSuccess,
    setUserSuccess,
} = authSlice.actions

export default authSlice.reducer
